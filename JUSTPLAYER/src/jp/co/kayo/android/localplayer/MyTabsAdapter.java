
package jp.co.kayo.android.localplayer;

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.fragment.ControlFragment;
import jp.co.kayo.android.localplayer.fragment.MainFragment;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Logger;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.TextUtils.TruncateAt;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import android.app.ActionBar;
import android.app.ActionBar.Tab;

public class MyTabsAdapter extends FragmentPagerAdapter implements
        ViewPager.OnPageChangeListener, ActionBar.TabListener, OnClickListener,
        OnLongClickListener {
    private final MainActivity2 mMain;
    private final ActionBar mActionBar;
    private final ViewPager mViewPager;
    private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();
    private ColorSet mColorset;
    private SharedPreferences mPref;

    static final class TabInfo {
        final String tag;
        final Class<?> clss;
        final Bundle args;
        String fragment_tag;

        TabInfo(String _tag, Class<?> _class, Bundle _args) {
            tag = _tag;
            clss = _class;
            args = _args;
        }
    }

    public MyTabsAdapter(MainActivity2 activity, ActionBar actionBar,
            ViewPager pager) {
        super(activity.getSupportFragmentManager());
        mMain = activity;
        mActionBar = actionBar;
        mViewPager = pager;
        mViewPager.setAdapter(this);
        mViewPager.setOnPageChangeListener(this);
        mColorset = new ColorSet();
        mColorset.load(activity);
        mPref = PreferenceManager.getDefaultSharedPreferences(activity);
    }

    public void addTab(ActionBar.Tab tab, Class<?> clss, Bundle args) {
        String tag = tab.getText().toString();
        TabInfo info = new TabInfo(tag, clss, args);
        mTabs.add(info);
        tab.setTag(info);
        String tabname = null;
        Fragment f = mMain.getSupportFragmentManager().findFragmentByTag(info.fragment_tag);
        if (f != null && f instanceof ContentManager) {
            tabname = ((ContentManager) f).getName(mMain);
        }

        View view = createTabView(mMain, tabname != null ? tabname : tab.getText()
                .toString());
        view.setTag(tab);
        tab.setCustomView(view);
        mActionBar.addTab(tab.setTabListener(this));
        notifyDataSetChanged();
    }

    public void setTabText(ActionBar.Tab tab, String text) {
        TextView view = (TextView) tab.getCustomView();
        if (view != null) {
            view.setText(text);
            mMain.mHandler.sendEmptyMessage(SystemConsts.EVT_UPDATE_LISTVIEW);
        }
    }

    private View createTabView(Context context, String text) {
        if (text != null) {
            TextView textView = new TextView(context, null, android.R.attr.actionBarTabTextStyle);
            //TextView textView = new TextView(context, null, R.attr.actionBarTabTextStyle);
            textView.setEllipsize(TruncateAt.END);
            LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.CENTER_VERTICAL;
            textView.setLayoutParams(lp);
            textView.setGravity(Gravity.CENTER_VERTICAL);
            textView.setText(text);
            int color = mColorset.getColor(ColorSet.KEY_ACTIONBAR_TAB_COLOR);
            if (color != -1) {
                textView.setTextColor(color);
            }

            textView.setOnClickListener(this);
            textView.setOnLongClickListener(this);

            return textView;
        }
        else {
            return null;
        }
    }

    public final ArrayList<TabInfo> getTabs() {
        return mTabs;
    }

    public String findFragmentTag(String tag) {
        for (TabInfo info : mTabs) {
            if (info.tag.equals(tag)) {
                return info.fragment_tag;
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

    @Override
    public Fragment getItem(int position) {
        TabInfo info = mTabs.get(position);
        Fragment fragment = Fragment.instantiate(mMain, info.clss.getName(),
                info.args);
        return fragment;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        MainFragment fragment = (MainFragment) super.instantiateItem(container,
                position);
        if (position < mTabs.size()) {
            TabInfo info = mTabs.get(position);
            info.fragment_tag = fragment.getTag();
        }
        return fragment;
    }

    public Fragment getFragment(int position) {
        Tab tab = mActionBar.getTabAt(position);
        TabInfo info = (TabInfo) tab.getTag();
        if (info != null) {
            return mMain.getSupportFragmentManager().findFragmentByTag(
                    info.fragment_tag);
        }
        return null;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset,
            int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        mMain.selectTab(position);
        ContentManager cmr = (ContentManager) getFragment(position);
        if (cmr != null) {
            cmr.changedMedia();
            FragmentManager m = mMain.getSupportFragmentManager();
            ControlFragment control = (ControlFragment) m
                    .findFragmentByTag(SystemConsts.TAG_CONTROL);
            if (control != null && control.getView() != null) {
                control.showControl(false);
            }
        }
        mMain.getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition());
        
    }

    @Override
    public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void onClick(View v) {
        Tab tab = mActionBar.getSelectedTab();
        View currelntView = tab.getCustomView();
        if (currelntView == v) {
            Logger.d("ReSelect Click!");
            Fragment f = mMain.mTabsAdapter.getFragment(tab.getPosition());
            if (f instanceof ContextMenuFragment) {
                String tabname = ((ContextMenuFragment) f).selectSort();
                if (tabname != null) {
                    mMain.mTabsAdapter.setTabText(tab, tabname);
                }
            }
        } else {
            Tab viewTab = (Tab) v.getTag();
            if (viewTab != null) {
                mActionBar.selectTab(viewTab);
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        final Tab tab = mActionBar.getSelectedTab();
        View currentView = tab.getCustomView();
        if (currentView == v) {
            Logger.d("ReSelect Long Click!");
            final int tabposotion = tab.getPosition();
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mMain);
            final CharSequence[] selectViews = {
                    mMain.getString(R.string.lb_tab_albums),
                    mMain.getString(R.string.lb_tab_albums_list),
                    mMain.getString(R.string.lb_tab_artist),
                    mMain.getString(R.string.lb_tab_artist_expand),
                    mMain.getString(R.string.lb_tab_media),
                    mMain.getString(R.string.lb_tab_playlist),
                    mMain.getString(R.string.lb_tab_folder),
                    mMain.getString(R.string.lb_tab_genres),
                    mMain.getString(R.string.lb_tab_videos),
                    mMain.getString(R.string.lb_tab_order)
            };
            final int[] selectKind = {
                    SystemConsts.TAG_ALBUM_GRID,
                    SystemConsts.TAG_ALBUM_LIST,
                    SystemConsts.TAG_ARTIST_LIST,
                    SystemConsts.TAG_ARTIST_EXPAND,
                    SystemConsts.TAG_MEDIA,
                    SystemConsts.TAG_PLAYLIST,
                    SystemConsts.TAG_FOLDER,
                    SystemConsts.TAG_GENRES,
                    SystemConsts.TAG_VIDEO,
                    SystemConsts.TAG_PLAYBACK
            };
            alertDialogBuilder.setTitle(mMain.getString(R.string.lb_tab_title));
            alertDialogBuilder.setItems(selectViews, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MainFragment f = (MainFragment) mMain.mTabsAdapter.getFragment(tabposotion);
                    String tabname = f.selectView(mMain, selectKind[which]);
                    if (tabname != null) {
                        mMain.mTabsAdapter.setTabText(tab, tabname);
                    }

                }
            });

            // ダイアログを表示
            alertDialogBuilder.create().show();
            return true;
        }
        return false;
    }


}
