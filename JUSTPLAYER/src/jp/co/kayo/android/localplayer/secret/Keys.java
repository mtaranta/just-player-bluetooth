package jp.co.kayo.android.localplayer.secret;

public interface Keys {
    //APP Billing
    public static final String SIGNATURE_ALGORITHM = "SHA1withRSA";

    public static final String[] HOLD_KEY = {"Base64Encoding API BuilingKey"};
    
    public static final String EXCEPTION_KEY = "ExceptionAPI Mail Service Key: http://androidexceptionmail.appspot.com/";
}
