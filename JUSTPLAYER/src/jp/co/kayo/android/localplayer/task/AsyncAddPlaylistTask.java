
package jp.co.kayo.android.localplayer.task;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.dialog.AddPlaylistDialog;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class AsyncAddPlaylistTask extends AsyncTask<Void, Void, Void> {
    private Context context;
    private FragmentManager fm;
    private long[] ids;
    private String where;
    private String[] whereArgs;
    private Uri uri;
    private String cname;
    private String order;
    
    public AsyncAddPlaylistTask(Context context, FragmentManager fm, String where, String[] whereArgs){
        this.context = context;
        this.fm = fm;
        this.where = where;
        this.whereArgs = whereArgs;
        this.uri = null;
    }

    public AsyncAddPlaylistTask(Context context, FragmentManager fm, Uri uri, String cname, String where, String[] whereArgs, String order) {
        this.context = context;
        this.fm = fm;
        this.uri = uri;
        this.cname = cname;
        this.where = where;
        this.whereArgs = whereArgs;
        this.order = order;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Cursor cursor = null;
        try {
            if(uri!=null){
                cursor = context.getContentResolver().query(uri,
                        new String[]{cname}, where, whereArgs, order);
            }
            else{
                cname = AudioMedia._ID;
                cursor = context.getContentResolver().query(
                        MediaConsts.MEDIA_CONTENT_URI,
                        new String[]{cname},
                        where,
                        whereArgs, AudioMedia.TRACK);
            }
            if (cursor != null && cursor.moveToFirst()) {
                int n = cursor.getCount();
                if (n > 0) {
                    ids = new long[n];
                    int i = 0;
                    do {
                        long id = cursor.getLong(0);
                        ids[i] = id;
                        i++;
                    } while (cursor.moveToNext());
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if(ids!=null){
            AddPlaylistDialog dlg = new AddPlaylistDialog();
            Bundle b = new Bundle();
            b.putLongArray("playlist", ids);
            dlg.setArguments(b);
            dlg.show(fm, SystemConsts.TAG_RATING_DLG);
        }
    }
}
