package jp.co.kayo.android.localplayer;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class SendRequestActivity extends Activity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_main);

        findViewById(R.id.requestBtnSendRequest).setOnClickListener(this);
        findViewById(R.id.requestBtnClose).setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.requestBtnSendRequest) {

            Uri uri = Uri.parse("mailto:justplayer-user@googlegroups.com");
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra(Intent.EXTRA_SUBJECT,
                    getString(R.string.mail_subject));
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.mail_text));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (v.getId() == R.id.requestBtnClose) {
            finish();
        }
    }
}