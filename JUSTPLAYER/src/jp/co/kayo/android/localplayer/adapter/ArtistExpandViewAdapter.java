package jp.co.kayo.android.localplayer.adapter;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Hashtable;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.core.ImageObserverImpl;
import jp.co.kayo.android.localplayer.core.IndexCursorAdapter;
import jp.co.kayo.android.localplayer.core.IndexTreeCursorAdapter;
import jp.co.kayo.android.localplayer.core.ViewHolder;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.ContentsUtils.AlbumInfo;
import jp.co.kayo.android.localplayer.util.FileUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.ImageObserver;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.FavoriteInfo;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class ArtistExpandViewAdapter extends IndexTreeCursorAdapter {
    LayoutInflater inflator;
    Context context;
    Handler handler = new Handler();
    Hashtable<String, AlbumInfo> tbl1 = new Hashtable<String, AlbumInfo>();
    Hashtable<String, AlbumInfo> tbl2 = new Hashtable<String, AlbumInfo>();
    String format;
    boolean hidealbumart = false;
    ViewCache viewCache;
    boolean searchExpand = false;

    int getColArtist(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioArtist.ARTIST);
    }

    int getColArtistKey(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioArtist.ARTIST_KEY);
    }

    int getColNumAlbum(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioArtist.NUMBER_OF_ALBUMS);
    }

    int getColNumTracks(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioArtist.NUMBER_OF_TRACKS);
    }

    int getColRating(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioArtist.FAVORITE_POINT);
    }

    public ArtistExpandViewAdapter(Context context, Cursor c, ViewCache cache) {
        super(context, c, true, MediaConsts.AudioArtist.ARTIST);
        this.context = context;
        this.viewCache = cache;
        format = context.getString(R.string.txt_artist_grid);
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        hidealbumart = pref.getBoolean("key.hideart", false);
    }

    public LayoutInflater getInflator(Context context) {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }
    
    @Override
    protected void bindChildView(View view, Context context, Cursor cursor, boolean isLastChild) {
        ViewHolder holder = (ViewHolder) view.getTag();
        long id = cursor.getLong(cursor
                .getColumnIndex(MediaConsts.AudioAlbum._ID));
        String album = cursor.getString(cursor
                .getColumnIndex(MediaConsts.AudioAlbum.ALBUM));
        String artist = cursor.getString(cursor
                .getColumnIndex(MediaConsts.AudioAlbum.ARTIST));
        String album_art = cursor.getString(cursor
                .getColumnIndex(MediaConsts.AudioAlbum.ALBUM_ART));
        int col = cursor.getColumnIndex(MediaConsts.AudioAlbum.FAVORITE_POINT);

        holder.setPotision(cursor.getPosition());
        if (MediaConsts.AudioAlbum.ALBUM.equals(getCname())) {
            holder.getText2().setText(album);
            holder.getText3().setText(artist);
        } else {
            holder.getText2().setText(artist);
            holder.getText3().setText(album);
        }

        if (col != -1) {
            holder.getRating1().setRating(cursor.getInt(col));
        } else {
            FavoriteInfo inf = viewCache.getFavorite(context, id,
                    TableConsts.FAVORITE_TYPE_ALBUM);
            holder.getRating1().setRating(inf.rating);
        }

        if (holder.getImage1() != null) {
            if (!imageloadskip) {
                Integer key = Funcs.getAlbumKey(album, artist);
                Bitmap bmp = viewCache.getImage(
                        album,
                        artist,
                        album_art,
                        new ImageObserverImpl(handler, viewCache, holder,
                                key, cursor.getPosition()));
                if (bmp != null) {
                    holder.getImage1().setImageBitmap(bmp);
                } else {
                    holder.getImage1().setImageBitmap(null);
                }
            } else {
                holder.getImage1().setImageBitmap(null);
            }
        }
        
    }

    @Override
    protected void bindGroupView(View view, Context context, Cursor cursor, boolean isExpanded) {
        ViewHolder holder = (ViewHolder) view.getTag();
        long id = cursor.getLong(cursor
                .getColumnIndex(MediaConsts.AudioArtist._ID));
        String artist = cursor.getString(getColArtist(cursor));
        String artistKey = cursor.getString(getColArtistKey(cursor));
        String nalbums = cursor.getString(getColNumAlbum(cursor));
        String ntracks = cursor.getString(getColNumTracks(cursor));
        int col = getColRating(cursor);

        holder.setPotision(cursor.getPosition());
        holder.getText2().setText(artist);
        holder.getText3().setText(String.format(format, nalbums, ntracks));

        if (col != -1) {
            holder.getRating1().setRating(cursor.getInt(col));
        } else {
            FavoriteInfo inf = viewCache.getFavorite(context, id,
                    TableConsts.FAVORITE_TYPE_ARTIST);
            holder.getRating1().setRating(inf.rating);
        }        
    }

    @Override
    protected Cursor getChildrenCursor(Cursor groupCursor) {
        String artist = groupCursor.getString(getColArtist(groupCursor));
        
        Cursor cursor =  context.getContentResolver().query(MediaConsts.ALBUM_CONTENT_URI,
                null, AudioAlbum.ARTIST + " like '%' || ? || '%' ", new String[]{artist}, AudioAlbum.FIRST_YEAR+","+AudioAlbum.LAST_YEAR);
        if(!searchExpand){
            return cursor;
        }
        else{
            if(cursor != null && cursor.getCount() > 0){
                return cursor;
            }else{
                Cursor media_cursor =  null;
                try{
                    int col = getColRating(cursor);
                    media_cursor = context.getContentResolver().query(MediaConsts.MEDIA_CONTENT_URI,
                            new String[]{MediaConsts.AudioMedia.ALBUM_KEY}, MediaConsts.AudioMedia.ARTIST + " like '%' || ? || '%' ", new String[]{artist}, null);
                    if(media_cursor != null && media_cursor.moveToFirst()){
                        HashSet<String> uniq = new HashSet<String>();
                        MatrixCursor retCursor = new MatrixCursor(
                                col !=-1? new String[]{AudioAlbum._ID, AudioAlbum.ALBUM, AudioAlbum.ALBUM_KEY, AudioAlbum.ARTIST, AudioAlbum.ALBUM_ART, AudioAlbum.FAVORITE_POINT}:
                                    new String[]{AudioAlbum._ID, AudioAlbum.ALBUM, AudioAlbum.ALBUM_KEY, AudioAlbum.ARTIST, AudioAlbum.ALBUM_ART});
                        do{
                            String albumKey = media_cursor.getString(media_cursor.getColumnIndex(MediaConsts.AudioMedia.ALBUM_KEY));
                            if(uniq.contains(albumKey)!=true){
                                uniq.add(albumKey);
                                addAlbumItem(albumKey, retCursor, col);
                            }
                            
                        }while(media_cursor.moveToNext());
                        return retCursor;
                    }else{
                        return null;
                    }
                }
                finally{
                    if(media_cursor != null){
                        media_cursor.close();
                    }
                }
            }
        }
    }
    
    private void addAlbumItem(String albumKey, MatrixCursor retCursor, int favcol){
        Cursor cursor =  null;
        try{
            cursor = context.getContentResolver().query(MediaConsts.ALBUM_CONTENT_URI,
                    favcol !=-1? new String[]{AudioAlbum._ID, AudioAlbum.ALBUM, AudioAlbum.ARTIST, AudioAlbum.ALBUM_ART, AudioAlbum.FAVORITE_POINT}:
                        new String[]{AudioAlbum._ID, AudioAlbum.ALBUM, AudioAlbum.ARTIST, AudioAlbum.ALBUM_ART}, AudioAlbum.ALBUM_KEY + " = ? ", new String[]{albumKey}, null);
            if(cursor != null && cursor.moveToFirst()){
                if(favcol !=-1){
                    retCursor.addRow(
                            new Object[]{
                                    cursor.getLong(cursor.getColumnIndex(AudioAlbum._ID)),
                                    cursor.getString(cursor.getColumnIndex(AudioAlbum.ALBUM)),
                                    albumKey,
                                    cursor.getString(cursor.getColumnIndex(AudioAlbum.ARTIST)),
                                    cursor.getString(cursor.getColumnIndex(AudioAlbum.ALBUM_ART)),
                                    cursor.getInt(cursor.getColumnIndex(AudioAlbum.FAVORITE_POINT))
                            });
                }
                else{
                    retCursor.addRow(
                            new Object[]{
                                    cursor.getLong(cursor.getColumnIndex(AudioAlbum._ID)),
                                    cursor.getString(cursor.getColumnIndex(AudioAlbum.ALBUM)),
                                    albumKey,
                                    cursor.getString(cursor.getColumnIndex(AudioAlbum.ARTIST)),
                                    cursor.getString(cursor.getColumnIndex(AudioAlbum.ALBUM_ART))
                            });
                }
            }
        }
        finally{
            if(cursor != null){
                cursor.close();
            }
        }
    }

    @Override
    protected View newChildView(Context context, Cursor cursor, boolean isLastChild,
            ViewGroup parent) {
        View v = getInflator(context).inflate(
                hidealbumart ? R.layout.text_expandlist_row4
                        : R.layout.img_expandlist_row_album, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.setImage1((ImageView) v.findViewById(R.id.imageArt));
        holder.setText2((TextView) v.findViewById(R.id.text2));
        viewCache.getColorset().setColor(ColorSet.KEY_DEFAULT_PRI_COLOR, holder.getText2());
        holder.setText3((TextView) v.findViewById(R.id.text3));
        viewCache.getColorset().setColor(ColorSet.KEY_DEFAULT_SEC_COLOR, holder.getText3());
        holder.setRating1((RatingBar) v.findViewById(R.id.ratingBar1));
        holder.setBackground(v.findViewById(R.id.background));
        v.setTag(holder);
        return v;
    }

    @Override
    protected View newGroupView(Context context, Cursor cursor, boolean isExpanded, ViewGroup parent) {
        View v = getInflator(context).inflate(R.layout.txt_expandlist_row_artist, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.setText2((TextView) v.findViewById(R.id.text2));
        viewCache.getColorset().setColor(ColorSet.KEY_DEFAULT_PRI_COLOR, holder.getText2());
        holder.setText3((TextView) v.findViewById(R.id.text3));
        viewCache.getColorset().setColor(ColorSet.KEY_DEFAULT_SEC_COLOR, holder.getText3());
        holder.setRating1((RatingBar) v.findViewById(R.id.ratingBar1));
        holder.setBackground(v.findViewById(R.id.background));
        v.setTag(holder);
        return v;
    }
    
    

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
            ViewGroup parent) {
        View view = super.getGroupView(groupPosition, isExpanded, convertView, parent);
        ViewHolder holder = (ViewHolder) view.getTag();
        if (groupPosition % 2 == 1) {
            holder.getBackground().setVisibility(View.GONE);
        } else {
            holder.getBackground().setVisibility(View.VISIBLE);
        }
        return view;
    }
    
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
            View convertView, ViewGroup parent) {
        View view = super.getChildView(groupPosition, childPosition, isLastChild, convertView, parent);
        ViewHolder holder = (ViewHolder) view.getTag();
        if (childPosition % 2 == 0) {
            holder.getBackground().setVisibility(View.GONE);
        } else {
            holder.getBackground().setVisibility(View.VISIBLE);
        }
        return view;
    }



    private class MyImageObserverImpl implements ImageObserver {

        ViewHolder mholder;
        Handler mHandler;
        Integer artistKey;
        Integer mKey;
        int mPotision;

        public MyImageObserverImpl(Handler handler, ViewCache viewcache,
                ViewHolder holder, Integer artistKey, Integer key, int pos) {
            this.mHandler = handler;
            this.mholder = holder;
            this.artistKey = artistKey;
            this.mKey = key;
            this.mPotision = pos;
        }

        @Override
        public void onLoadImage(final Bitmap bmp) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    File destFile = viewCache.createAlbumArtFile(artistKey);
                    File srcFile = viewCache.createAlbumArtFile(mKey);
                    if (srcFile.exists()) {
                        try {
                            FileUtils.copyFile(srcFile, destFile);
                        } catch (IOException e) {
                        }
                    }

                    if (mPotision == mholder.getPotision()) {
                        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
                        anim.setDuration(200);
                        mholder.getImage1().startAnimation(anim);
                        mholder.getImage1().setImageBitmap(bmp);
                        mholder.getImage1().invalidate();
                    }
                }
            });
        }

    }

}
