package jp.co.kayo.android.localplayer.core;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

public class ViewHolder {
    private int mPotision;
    private View mImgGrip;
    private ImageView mImage1;
    private TextView mText1;
    private TextView mText2;
    private TextView mText3;
    private TextView mText4;
    private TextView mText5;
    private RatingBar mRating1;
    private View mImgCache;
    private View mImgPlay;
    private ProgressBar mProgressBar1;
    private View mBackground;

    public View getImgGrip() {
        return mImgGrip;
    }

    public void setImgGrip(View imgGrip) {
        mImgGrip = imgGrip;
    }

    public RatingBar getRating1() {
        return mRating1;
    }

    public void setRating1(RatingBar rating1) {
        mRating1 = rating1;
    }

    public View getImgCache() {
        return mImgCache;
    }

    public void setImgCache(View imgCache) {
        mImgCache = imgCache;
    }

    public View getImgPlay() {
        return mImgPlay;
    }

    public void setImgPlay(View imgPlay) {
        mImgPlay = imgPlay;
    }

    public ProgressBar getProgressBar1() {
        return mProgressBar1;
    }

    public void setProgressBar1(ProgressBar progressBar1) {
        mProgressBar1 = progressBar1;
    }


    public synchronized void setPotision(int potision) {
        mPotision = potision;
    }
    
    public synchronized int getPotision() {
        return mPotision;
    }

    public ImageView getImage1() {
        return mImage1;
    }

    public void setImage1(ImageView image1) {
        mImage1 = image1;
    }

    public TextView getText1() {
        return mText1;
    }

    public void setText1(TextView text1) {
        mText1 = text1;
    }

    public TextView getText2() {
        return mText2;
    }

    public void setText2(TextView text2) {
        mText2 = text2;
    }

    public TextView getText3() {
        return mText3;
    }

    public void setText3(TextView text3) {
        mText3 = text3;
    }

    public TextView getText4() {
        return mText4;
    }

    public void setText4(TextView text4) {
        mText4 = text4;
    }

    public TextView getText5() {
        return mText5;
    }

    public void setText5(TextView text5) {
        mText5 = text5;
    }

    public View getBackground() {
        return mBackground;
    }

    public void setBackground(View background) {
        mBackground = background;
    }

}
