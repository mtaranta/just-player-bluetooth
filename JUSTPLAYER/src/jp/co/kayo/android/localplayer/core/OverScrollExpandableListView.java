package jp.co.kayo.android.localplayer.core;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ExpandableListView;

public class OverScrollExpandableListView extends ExpandableListView {

    public OverScrollExpandableListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public OverScrollExpandableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OverScrollExpandableListView(Context context) {
        super(context);
    }

    @TargetApi(9)
    @Override
    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX,
            int scrollY, int scrollRangeX, int scrollRangeY,
            int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
        return super.overScrollBy(deltaX, deltaY, scrollX, scrollY,
                scrollRangeX, scrollRangeY, maxOverScrollX, 100, isTouchEvent);
    }
}
