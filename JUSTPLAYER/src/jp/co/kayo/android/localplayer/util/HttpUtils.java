
package jp.co.kayo.android.localplayer.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class HttpUtils {

    public JSONObject getJSON(String path) throws JSONException, IOException {

        InputStream in = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(path.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(20000);
            conn.setReadTimeout(20000);
            int responseCode = conn.getResponseCode();
            if (responseCode >= 200 && responseCode <= 206) {
                in = conn.getInputStream();

                String jsonstr = new String(getByteArrayFromStream(in));
                Log.d("KobekonClient", jsonstr);
                return new JSONObject(jsonstr);
            }
            else {
                throw new IOException("URL Connection Error :" + responseCode);
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public JSONArray getJSONArray(String path) throws JSONException, IOException {

        InputStream in = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(path.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(20000);
            conn.setReadTimeout(20000);
            int responseCode = conn.getResponseCode();
            if (responseCode >= 200 && responseCode <= 206) {
                in = conn.getInputStream();

                String jsonstr = new String(getByteArrayFromStream(in));
                return new JSONArray(jsonstr);
            }
            else {
                throw new IOException("URL Connection Error :" + responseCode);
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public byte[] getByteArrayFromStream(InputStream in) {
        byte[] line = new byte[512];
        byte[] result = null;
        ByteArrayOutputStream out = null;
        int size = 0;
        try {
            out = new ByteArrayOutputStream();
            BufferedInputStream bis = new BufferedInputStream(in);
            while (true) {
                size = bis.read(line);
                if (size < 0) {
                    break;
                }
                out.write(line, 0, size);
            }
            result = out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (Exception e) {
            }
        }
        return result;
    }
}
