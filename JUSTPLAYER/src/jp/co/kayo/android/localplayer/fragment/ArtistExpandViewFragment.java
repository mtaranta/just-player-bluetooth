
package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.util.ArrayList;

import jp.co.kayo.android.localplayer.AlbumartActivity;
import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseExpandListFragment;
import jp.co.kayo.android.localplayer.BaseFragment;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TabFragment;
import jp.co.kayo.android.localplayer.TagEditActivity;
import jp.co.kayo.android.localplayer.adapter.ArtistExpandViewAdapter;
import jp.co.kayo.android.localplayer.adapter.ArtistListViewAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.FastOnTouchListener;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.IndexTreeCursorAdapter;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.MediaData;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.support.v4.content.CursorLoader;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class ArtistExpandViewFragment extends BaseExpandListFragment implements
        ContentManager, TabFragment,
        ContextMenuFragment, LoaderCallbacks<Cursor>, OnScrollListener, OnChildClickListener, OnItemLongClickListener, OnGroupClickListener {
    private ViewCache viewcache;
    private SharedPreferences mPref;
    ArtistExpandViewAdapter mAdapter;
    private ActionMode mMode;
    int mSelectedPosition;

    boolean isReadFooter;
    Runnable mTask = null;
    MatrixCursor mcur;
    AsyncTask<Void, Void, Void> loadtask = null;
    boolean getall = false;
    private String mQueryString;

    @Override
    protected void messageHandle(int what, int selectedGroupPosition, int selectedChildPosition) {
        Cursor selectedCursor = null;
        if(selectedChildPosition!=-1){
            selectedCursor = mAdapter.getChild(selectedGroupPosition, selectedChildPosition);
            if (selectedCursor != null) {
                long mediaId = selectedCursor.getLong(selectedCursor
                        .getColumnIndex(AudioAlbum._ID));
                String albumKey = selectedCursor.getString(selectedCursor
                        .getColumnIndex(AudioAlbum.ALBUM_KEY));

                switch (what) {
                    case SystemConsts.EVT_SELECT_RATING: {
                        RatingDialog dlg = new RatingDialog(getActivity(),
                                TableConsts.FAVORITE_TYPE_ALBUM, mediaId, mHandler);
                        dlg.show();
                    }
                        break;
                    case SystemConsts.EVT_SELECT_ARTIST: {
                        String artist = selectedCursor.getString(selectedCursor
                                .getColumnIndex(AudioAlbum.ARTIST));

                        FragmentTransaction t = getFragmentManager().beginTransaction();
                        AnimationHelper.setFragmentToPlayBack(t);
                        t.add(FragmentUtils.getLayoutId(getActivity(), this),
                                ArtistListFragment
                                        .createFragment(null, artist, -1, FragmentUtils.cloneBundle(this)));
                        t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                        t.hide(this);
                        t.commit();
                    }
                        break;
                    case SystemConsts.EVT_SELECT_ADD: {
                        AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(getActivity(),
                                getFragmentManager(), AudioMedia.ALBUM_KEY + " = ?", new String[] {
                                        albumKey
                                });
                        task.execute();
                    }
                        break;
                    case SystemConsts.EVT_SELECT_MORE: {
                        String artist = selectedCursor.getString(selectedCursor
                                .getColumnIndex(AudioAlbum.ARTIST));
                        if (artist != null) {
                            showInfoDialog(null, artist, null);
                        }
                    }
                        break;
                    case SystemConsts.EVT_SELECT_ALBUMART: {
                        Intent i = new Intent(getActivity(), AlbumartActivity.class);
                        i.putExtra("album_key", albumKey);
                        getActivity().startActivity(i);
                    }
                        break;
                    case SystemConsts.EVT_SELECT_CLEARCACHE: {
                        Cursor cursor = null;
                        try {
                            ContentsUtils.clearAlbumCache(getActivity(), albumKey);
                            ContentValues values = new ContentValues();
                            values.put(AudioAlbum.ALBUM_INIT_FLG, 0);
                            getActivity().getContentResolver().update(
                                    ContentUris.withAppendedId(
                                            MediaConsts.ALBUM_CONTENT_URI, mediaId),
                                    values, null, null);
                        } finally {
                            if (cursor != null) {
                                cursor.close();
                            }
                            reload();
                        }
                    }
                        break;
                    case SystemConsts.EVT_SELECT_EDIT: {
                        Intent intent = new Intent(getActivity(), TagEditActivity.class);
                        intent.putExtra(SystemConsts.KEY_EDITTYPE,
                                TagEditActivity.ALBUM);
                        intent.putExtra(SystemConsts.KEY_EDITKEY, albumKey);
                        getActivity().startActivityForResult(intent, SystemConsts.REQUEST_TAGEDIT);
                    }
                        break;
                    default: {
                    }
                }
            }
        }
        else {
            selectedCursor = mAdapter.getGroup(selectedGroupPosition);
            if (selectedCursor != null) {
                long mediaId = selectedCursor.getLong(selectedCursor
                        .getColumnIndex(AudioArtist._ID));
                String artistKey = selectedCursor.getString(selectedCursor
                        .getColumnIndex(AudioArtist.ARTIST_KEY));

                switch (what) {
                    case SystemConsts.EVT_SELECT_RATING: {
                        RatingDialog dlg = new RatingDialog(getActivity(),
                                TableConsts.FAVORITE_TYPE_ARTIST, mediaId, mHandler);
                        dlg.show();
                    }
                        break;
                    case SystemConsts.EVT_SELECT_ARTIST: {
                        String artist = selectedCursor.getString(selectedCursor
                                .getColumnIndex(AudioArtist.ARTIST));
                        if (artist != null) {
                            FragmentTransaction t = getFragmentManager().beginTransaction();
                            AnimationHelper.setFragmentToPlayBack(t);
                            t.add(FragmentUtils.getFragmentId(getActivity(), this),
                                    jp.co.kayo.android.localplayer.fragment.ArtistListFragment
                                            .createFragment(null, artist, -1, FragmentUtils.cloneBundle(this)));
                            t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                            t.hide(this);
                            t.commit();
                        }
                    }
                        break;
                    case SystemConsts.EVT_SELECT_ADD: {
                        AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(getActivity(),
                                getFragmentManager(), MediaConsts.AudioMedia.ARTIST_KEY + " = ?",
                                new String[] {
                                    artistKey
                                });
                        task.execute();
                    }
                        break;
                    case SystemConsts.EVT_SELECT_MORE: {
                        String artist = selectedCursor.getString(selectedCursor
                                .getColumnIndex(AudioArtist.ARTIST));
                        if (artist != null) {
                            showInfoDialog(null, artist, null);
                        }
                    }
                        break;
                    case SystemConsts.EVT_SELECT_CLEARCACHE: {
                        Cursor cursor = null;
                        try {
                            cursor = getActivity().getContentResolver().query(
                                    MediaConsts.MEDIA_CONTENT_URI,
                                    null,
                                    MediaConsts.AudioMedia.ARTIST_KEY + " = ?",
                                    new String[] {
                                        artistKey
                                    },
                                    MediaConsts.AudioMedia.ALBUM + ","
                                            + MediaConsts.AudioMedia.TRACK);
                            if (cursor != null && cursor.moveToFirst()) {
                                do {
                                    String title = cursor
                                            .getString(cursor
                                                    .getColumnIndex(MediaConsts.AudioMedia.TITLE));
                                    String data = cursor
                                            .getString(cursor
                                                    .getColumnIndex(MediaConsts.AudioMedia.DATA));
                                    File file = StreamCacherServer.getCacheFile(
                                            getActivity(), data);
                                    if (file != null && file.exists()) {
                                        file.delete();
                                        Toast.makeText(
                                                getActivity(),
                                                getString(R.string.txt_action_deletecache)
                                                        + "(" + title + ")",
                                                Toast.LENGTH_SHORT).show();
                                        mAdapter.notifyDataSetChanged();
                                    }
                                } while (cursor.moveToNext());
                            }
                        } finally {
                            if (cursor != null) {
                                cursor.close();
                            }
                        }
                    }
                        break;
                    case SystemConsts.EVT_SELECT_EDIT: {
                        Intent intent = new Intent(getActivity(), TagEditActivity.class);
                        intent.putExtra(SystemConsts.KEY_EDITTYPE,
                                TagEditActivity.ARTIST);
                        intent.putExtra(SystemConsts.KEY_EDITKEY, artistKey);
                        getActivity().startActivityForResult(intent, SystemConsts.REQUEST_TAGEDIT);

                    }
                        break;
                    default: {
                    }
                }
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (savedInstanceState != null) {
            getall = savedInstanceState.getBoolean("getall");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("getall", getall);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.artistart_expandlist_view, container,
                false);

        ExpandableListView list = (ExpandableListView) root
                .findViewById(android.R.id.list);
        list.setOnChildClickListener(this);
        list.setOnGroupClickListener(this);
        list.setOnScrollListener(this);
        list.setOnItemLongClickListener(this);
        isReadFooter = ContentsUtils.isNoCacheAction(mPref);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
        if (savedInstanceState != null && getFragmentManager() != null) {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                FragmentTransaction t = getFragmentManager().beginTransaction();
                t.hide(this);
                t.commit();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View view,
            int position, long arg3) {
        long packed = getListView().getExpandableListPosition(position);
        int group = ExpandableListView.getPackedPositionGroup(packed);
        int child = ExpandableListView.getPackedPositionChild(packed);
        int type = ExpandableListView.getPackedPositionType(packed);
        
        Cursor selectedCursor = (Cursor) getListView().getItemAtPosition(position);
        if(type == 0){
            BaseActivity act = (BaseActivity) getActivity();
            mMode = act.startActionMode(new AnActionModeOfEpicProportions(
                    getActivity(), group, -1, mHandler));
            mMode.setTitle(selectedCursor.getString(selectedCursor
                    .getColumnIndex(AudioArtist.ARTIST)));
            mMode.setSubtitle(getString(R.string.lb_tab_artist));
            return true;
        }
        else{
            BaseActivity act = (BaseActivity) getActivity();
            mMode = act.startActionMode(new AnActionModeOfEpicProportions(
                    getActivity(), group, child, mHandler));
            mMode.setTitle(selectedCursor.getString(selectedCursor
                    .getColumnIndex(AudioAlbum.ALBUM)));
            mMode.setSubtitle(getString(R.string.lb_tab_albums));
            return true; 
        }
    }

    private final class AnActionModeOfEpicProportions extends
            MediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                int selectedParent, int selectedChild, Handler handler) {
            super(context, selectedParent, selectedChild, handler);
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mMode = null;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_rating))
                    .setIcon(R.drawable.ic_menu_star)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            
            if(mSelectedChild == -1){
                menu.add(getString(R.string.sub_mnu_artist))
                .setIcon(R.drawable.ic_menu_btn_artist)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }

            menu.add(getString(R.string.txt_web_more))
                    .setIcon(R.drawable.ic_menu_wikipedia)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            // 文字化け修正メニューを追加
            menu.add(getString(R.string.sub_mnu_edit))
                    .setIcon(R.drawable.ic_menu_strenc)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            
            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_delete)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }

            return true;
        }

    };


    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        return false;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
            int childPosition, long id) {
        Cursor cursor = (Cursor) mAdapter.getChild(groupPosition, childPosition);
        if (cursor != null) {
            String album_key = cursor.getString(cursor
                    .getColumnIndex(AudioAlbum.ALBUM_KEY));

            FragmentTransaction t = getFragmentManager().beginTransaction();
            AnimationHelper.setFragmentToPlayBack(t);
            t.add(FragmentUtils.getFragmentId(getActivity(), this), AlbumSongsFragment.createFragment(album_key, FragmentUtils.cloneBundle(this)));
            t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
            t.hide(this);
            t.commit();
        }
        
        return false;
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void changedMedia() {

    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }

    @Override
    public int getFragmentId() {
        return R.layout.artistart_expandlist_view;
    }

    public String getSortOrder() {
        return MediaConsts.AudioArtist.ARTIST;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder = getSortOrder();
        if (mAdapter == null) {
            mAdapter = new ArtistExpandViewAdapter(getActivity(), null, viewcache);
            getListView().setOnTouchListener(
                    new IndexTreeCursorAdapter.FastOnTouchListener(new Handler(), mAdapter));
            setListAdapter(mAdapter);
        } else {
            mAdapter.changeCursor(null);
        }

        if (Funcs.isNotEmpty(mQueryString)) {
            selection = MediaConsts.AudioMedia.ARTIST + " like '%' || ? || '%'";
            selectionArgs = new String[] {
                mQueryString
            };
        }

        getall = false;
        showProgressBar();
        return new CursorLoader(getActivity(), MediaConsts.ARTIST_CONTENT_URI,
                null, selection, selectionArgs, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        try {
            hideProgressBar();
            if (mAdapter != null) {
                if (data == null || data.isClosed()) {
                    if(data==null) mAdapter.changeCursor(null);
                    invisibleProgressBar();
                    return;
                }
                if (ContentsUtils.isNoCacheAction(mPref)) {
                    int count = data.getCount();
                    try {
                        mcur = new MatrixCursor(data.getColumnNames(),
                                data.getCount());
                        if (count > 0) {
                            data.moveToFirst();
                            do {
                                ArrayList<Object> values = new ArrayList<Object>();
                                for (int i = 0; i < mcur.getColumnCount(); i++) {
                                    if (AudioArtist.NUMBER_OF_ALBUMS
                                            .equals(mcur.getColumnName(i))
                                            || AudioArtist.NUMBER_OF_TRACKS
                                                    .equals(mcur
                                                            .getColumnName(i))) {
                                        values.add(data.getInt(i));
                                    } else {
                                        values.add(data.getString(i));
                                    }
                                }
                                mcur.addRow(values
                                        .toArray(new Object[values.size()]));
                            } while (data.moveToNext());
                            mAdapter.changeCursor(mcur);
                        }

                    } finally {
                        if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                            invisibleProgressBar();
                        }
                        if (data != null) {
                            data.close();
                        }
                    }
                } else {
                    mAdapter.changeCursor(data);
                    invisibleProgressBar();
                }
            }
        } finally {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
        mcur = null;
    }

    public static final String[] fetchcols = new String[] {
            AudioArtist._ID,
            AudioArtist.ARTIST, AudioArtist.ARTIST_KEY,
            AudioArtist.NUMBER_OF_ALBUMS, AudioArtist.NUMBER_OF_TRACKS
    };

    private void invisibleProgressBar() {
        getall = true;
        mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        if (mAdapter == null || loadtask != null) {
            return;
        }
        if (isReadFooter) {
            if (!getall && totalItemCount > 0
                    && totalItemCount == (firstVisibleItem + visibleItemCount)) {
                final int current = mAdapter.getGroupCount();
                if (current > 0) {
                    loadtask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            Cursor cursor = null;
                            try {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_SHOWPROGRESS);
                                cursor = getActivity()
                                        .getContentResolver()
                                        .query(MediaConsts.ARTIST_CONTENT_URI,
                                                fetchcols,
                                                "LIMIT ? AND OFFSET ?",
                                                new String[] {
                                                        Integer.toString(SystemConsts.DEFAULT_FETCH_LIMIT),
                                                        Long.toString(current)
                                                },
                                                null);
                                int count = cursor.getCount();
                                if (mcur != null && count > 0) {
                                    cursor.moveToFirst();
                                    do {
                                        ArrayList<Object> values = new ArrayList<Object>();
                                        for (int i = 0; i < mcur
                                                .getColumnCount(); i++) {
                                            String colname = mcur
                                                    .getColumnName(i);
                                            int colid = cursor
                                                    .getColumnIndex(colname);
                                            if (colid != -1) {
                                                if (AudioArtist.NUMBER_OF_ALBUMS
                                                        .equals(colname)
                                                        || AudioArtist.NUMBER_OF_TRACKS
                                                                .equals(colname)) {
                                                    values.add(cursor
                                                            .getInt(colid));
                                                } else {
                                                    values.add(cursor
                                                            .getString(colid));
                                                }
                                            }
                                        }
                                        mHandler.sendMessage(mHandler
                                                .obtainMessage(
                                                        SystemConsts.ACT_ADDROW,
                                                        values.toArray(new Object[values
                                                                .size()])));
                                    } while (cursor.moveToNext());

                                    if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                                        invisibleProgressBar();
                                    }
                                } else {
                                    if (count < 1) {
                                        invisibleProgressBar();
                                    }
                                }
                            } finally {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
                                if (cursor != null) {
                                    cursor.close();
                                }
                                mHandler.sendEmptyMessage(SystemConsts.ACT_NOTIFYDATASETCHANGED);
                                loadtask = null;
                            }

                            return null;
                        }
                    };
                    loadtask.execute((Void) null);
                }
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = Funcs.getHideTime(mPref);
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public String selectSort() {
        return null;
    }

    @Override
    public boolean onBackPressed() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
            return true;
        }
        return false;
    }

    @Override
    protected void datasetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void addRow(Object[] values) {
        if (mcur != null) {
            mcur.addRow(values);
        }
    }

    @Override
    protected void hideMenu() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_artist_expand);
    }

    @Override
    public void doSearchQuery(String queryString) {
        boolean dirty = mQueryString == null || !mQueryString.equals(queryString);
        if (dirty) {
            mQueryString = queryString;
            reload();
        }
    }

}
