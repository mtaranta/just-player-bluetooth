package jp.co.kayo.android.localplayer.fragment;

import android.app.ActionBar.Tab;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.MainActivity2;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MainFragment extends Fragment implements ContentManager, ContextMenuFragment {
    SharedPreferences mPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(FragmentUtils.getLayoutId(getActivity(), this), null);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        int position = FragmentUtils.getTabPosition(this);
        MainActivity2 activity = (MainActivity2)getActivity();
        Tab tab = activity.getSupportActionBar().getTabAt(position);
        String tabname = selectView(getActivity(), FragmentUtils.getTabKind(getActivity(), this));
        activity.mTabsAdapter.setTabText(tab, tabname);
    }
    
    
    
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    
    public String selectView(Context context, int kind){
        Fragment f = null;
        if(kind == SystemConsts.TAG_ALBUM_GRID){
            f = new AlbumGridViewFragment();
        }
        else if(kind == SystemConsts.TAG_ALBUM_LIST){
            f = new AlbumListViewFragment();
        }
        else if(kind == SystemConsts.TAG_ARTIST_LIST){
            f = new ArtistListViewFragment();
        }
        else if(kind == SystemConsts.TAG_ARTIST_EXPAND){
            f = new ArtistExpandViewFragment();
        }
        else if(kind == SystemConsts.TAG_MEDIA){
            f = new MediaListViewFragment();
        }
        else if(kind == SystemConsts.TAG_PLAYLIST){
            f = new PlaylistViewFragment();
        }
        else if(kind == SystemConsts.TAG_FOLDER){
            f = new FileListViewFragment();
        }
        else if(kind == SystemConsts.TAG_GENRES){
            f = new GenresListViewFragment();
        }
        else if(kind == SystemConsts.TAG_VIDEO){
            f = new VideoListViewFragment();
        }
        else if(kind == SystemConsts.TAG_PLAYBACK){
            f = new PlaybackListViewFragment();
        }
        else {
            kind = SystemConsts.TAG_BLANK;
        }
        FragmentUtils.saveTabKind(context, this, kind);
        
        if(f!=null){
            Bundle args = FragmentUtils.cloneBundle(this);
            f.setArguments(args);
            if(getFragmentManager().getBackStackEntryCount()>0){
                getFragmentManager().popBackStack();
            }
            FragmentTransaction t = getFragmentManager().beginTransaction();
            t.replace(FragmentUtils.getFragmentId(context, this), f);
            t.commit();
            
            if(f instanceof ContentManager){
                return ((ContentManager)f).getName(context);
            }
        }
        return context.getString(R.string.lb_tab_blank);
    }

    @Override
    public void reload() {
        ContentManager mgr = (ContentManager)getFragmentManager().findFragmentById(FragmentUtils.getFragmentId(getActivity(), this));
        if(mgr!=null){
            Logger.d("Reload Class="+mgr.getClass().getName());
            mgr.reload();
        }
    }

    @Override
    public void changedMedia() {
        ContentManager mgr = (ContentManager)getFragmentManager().findFragmentById(FragmentUtils.getFragmentId(getActivity(), this));
        if(mgr!=null){
            mgr.changedMedia();
        }
    }

    @Override
    public void release() {
        ContentManager mgr = (ContentManager)getFragmentManager().findFragmentById(FragmentUtils.getFragmentId(getActivity(), this));
        if(mgr!=null){
            mgr.release();
        }
    }

    @Override
    public String getName(Context context) {
        ContentManager mgr = (ContentManager)getFragmentManager().findFragmentById(FragmentUtils.getFragmentId(getActivity(), this));
        if(mgr!=null){
            return mgr.getName(context);
        }
        return null;
    }
    
    @Override
    public boolean onBackPressed() {
        Fragment f = getFragmentManager().findFragmentById(FragmentUtils.getFragmentId(getActivity(), this));
        if(f instanceof ContextMenuFragment){
            return ((ContextMenuFragment)f).onBackPressed();
        }
        return false;
    }

    @Override
    public String selectSort() {
        Fragment f = getFragmentManager().findFragmentById(FragmentUtils.getFragmentId(getActivity(), this));
        if(f instanceof ContextMenuFragment){
            return ((ContextMenuFragment)f).selectSort();
        }
        return null;
    }

    @Override
    public void doSearchQuery(String queryString) {
        Fragment f = getFragmentManager().findFragmentById(FragmentUtils.getFragmentId(getActivity(), this));
        if(f instanceof ContextMenuFragment){
            ((ContextMenuFragment)f).doSearchQuery(queryString);
        }
    }
}
