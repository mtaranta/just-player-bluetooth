package jp.co.kayo.android.localplayer.provider;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.net.MalformedURLException;

import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.db.JustPlayerDatabaseHelper;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;
import jp.co.kayo.android.localplayer.util.Logger;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;
import android.provider.BaseColumns;

public class DownloadHelper {
    DownloadManager downLoadManager = null;
    // File videodir = new File(Environment.getExternalStorageDirectory(),
    // "data/jp.co.kayo.android.localplayer/cache/video/");
    // File musicdir = new File(Environment.getExternalStorageDirectory(),
    // "data/jp.co.kayo.android.localplayer/cache/music/");

    Context context;

    public DownloadHelper(Context context) {
        this.context = context;
    }

    public DownloadManager getManager() {
        if (downLoadManager == null) {
            downLoadManager = (DownloadManager) context
                    .getSystemService(Activity.DOWNLOAD_SERVICE);
        }
        return downLoadManager;
    }

    private String getName(String data) {
        return new File(data).getName();
    }

    public long enqueueVideo(String title, String mime, String data)
            throws MalformedURLException {

        String uri = StreamCacherServer.getContentUri(context, data);
        if (uri != null) {
            String name = getName(title);

            DownloadManager downLoadManager_ = getManager();

            DownloadManager.Request request = new DownloadManager.Request(
                    Uri.parse(uri));
            request.setAllowedOverRoaming(false);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE
                    | DownloadManager.Request.NETWORK_WIFI);
            request.setMimeType(mime);

            request.setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_MOVIES, name);
            File pathExternalPublicDir = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
            if (!pathExternalPublicDir.exists()) {
                pathExternalPublicDir.mkdirs();
            }

            File target = new File(pathExternalPublicDir, name);
            Logger.d("local:" + target.getAbsolutePath());

            request.setShowRunningNotification(true);
            request.setVisibleInDownloadsUi(true);
            request.setTitle(title);
            request.setDescription(title);

            long id = downLoadManager_.enqueue(request);

            /*
             * ContentValues values = new ContentValues();
             * values.put(TableConsts.DOWNLOAD_ID, id);
             * values.put(TableConsts.DOWNLOAD_MEDIA_ID, data.hashCode());
             * values.put(TableConsts.DOWNLOAD_TITLE, title);
             * values.put(TableConsts.DOWNLOAD_LOCAL_URI,
             * target.getAbsolutePath()); values.put(TableConsts.DOWNLOAD_TYPE,
             * 1); values.put(TableConsts.DOWNLOAD_STATUS,
             * DownloadManager.STATUS_PENDING);
             * context.getContentResolver().insert
             * (MediaConsts.DOWNLOAD_CONTENT_URI, values);
             */

            return id;
        } else {
            return -1;
        }
    }

    public long enqueueMusic(long mediaId, String title, String data)
            throws MalformedURLException {
        File cacheFile = StreamCacherServer.getCacheFile(context, data);
        if (cacheFile != null && !cacheFile.exists()) {
            String uri = StreamCacherServer.getContentUri(context, data);
            if (uri != null) {
                String fname = "media" + getName(data).hashCode();
                DownloadManager downLoadManager_ = getManager();

                DownloadManager.Request request = new DownloadManager.Request(
                        Uri.parse(uri));
                request.setAllowedOverRoaming(false);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE
                        | DownloadManager.Request.NETWORK_WIFI);

                request.setDestinationInExternalPublicDir(
                        Environment.DIRECTORY_MUSIC, fname);
                File pathExternalPublicDir = Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
                if (!pathExternalPublicDir.exists()) {
                    boolean b = pathExternalPublicDir.mkdirs();
                    Logger.d("pathExternalPublicDir="
                            + pathExternalPublicDir.getPath() + " mkdirs=" + b);
                }

                request.setShowRunningNotification(true);
                request.setVisibleInDownloadsUi(true);
                request.setTitle(title);
                request.setDescription(title);

                long id = downLoadManager_.enqueue(request);

                Logger.d("down id:" + id);
                Logger.d("data:" + data);
                Logger.d("local:" + fname);

                ContentValues values = new ContentValues();
                values.put(BaseColumns._ID, id);
                values.put(TableConsts.DOWNLOAD_ID, id);
                values.put(TableConsts.DOWNLOAD_MEDIA_ID, mediaId);
                values.put(TableConsts.DOWNLOAD_TITLE, title);
                values.put(TableConsts.DOWNLOAD_LOCAL_URI, fname);
                values.put(TableConsts.DOWNLOAD_REMOTE_URI,
                        cacheFile.getAbsolutePath());
                values.put(TableConsts.DOWNLOAD_TYPE, 0); // 0:music 1:video
                values.put(TableConsts.DOWNLOAD_STATUS,
                        DownloadManager.STATUS_PENDING);
                JustPlayerDatabaseHelper helper = null;
                SQLiteDatabase db = null;
                try {
                    helper = new JustPlayerDatabaseHelper(context);
                    db = helper.getWritableDatabase();
                    db.beginTransaction();
                    long insid = db.insert(TableConsts.TBNAME_DOWNLOAD, null,
                            values);
                    db.setTransactionSuccessful();
                    return id;
                } finally {
                    if (db != null) {
                        db.endTransaction();
                        db.close();
                    }
                    if (helper != null) {
                        helper.close();
                    }
                }
            }
        }
        return -1;
    }

    public void print(long id) {
        DownloadManager mgr = getManager();
        Cursor c = null;
        try {
            Query q = new Query();
            q.setFilterById(id);
            // q.setFilterByStatus(DownloadManager.STATUS_SUCCESSFUL);

            c = mgr.query(q);

            if (c != null && c.moveToFirst()) {
                for (int i = 0; i < c.getColumnCount(); i++) {
                    String cname = c.getColumnName(i);
                    String value = c.getString(i);
                    Logger.d(cname + " = " + value);

                }
                c.close();
            }
        } catch (CursorIndexOutOfBoundsException e) {
            c.close();
            return;
        }
    }
}
