package jp.co.kayo.android.localplayer.provider;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.db.JustPlayerDatabaseHelper;
import jp.co.kayo.android.localplayer.provider.MediaFile.MediaFileType;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.ID3Helper;
import jp.co.kayo.android.localplayer.util.Logger;
import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.UriMatcher;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaMetadataRetriever;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.provider.MediaStore;

/***
 * このクラスはMediaProvider（端末の音楽データ）のラッパーです
 * 
 * @author yokmama
 * 
 */
public class DeviceContentProvider extends ContentProvider implements
        MediaConsts {
    public static final String MEDIA_AUTHORITY = MediaConsts.AUTHORITY
            + ".media";
    public static final String MEDIA_CONTENT_AUTHORITY_SLASH = "content://"
            + MEDIA_AUTHORITY + "/";

    private UriMatcher uriMatcher = null;
    private SharedPreferences mPreference;
    private JustPlayerDatabaseHelper helper;

    final String[] media_fetchcols = new String[] { MediaConsts.AudioMedia._ID,
            MediaConsts.AudioMedia.TITLE, MediaConsts.AudioMedia.TITLE_KEY,
            MediaConsts.AudioMedia.ALBUM, MediaConsts.AudioMedia.ALBUM_KEY,
            MediaConsts.AudioMedia.ARTIST, MediaConsts.AudioMedia.ARTIST_KEY,
            MediaConsts.AudioMedia.DURATION, MediaConsts.AudioMedia.DATA,
            MediaConsts.AudioMedia.TRACK, MediaConsts.AudioMedia.YEAR, };

    final String[] MEDIA_PROJECTION = new String[] { AudioMedia._ID,
            AudioMedia.ALBUM, AudioMedia.ALBUM_KEY, AudioMedia.ARTIST,
            AudioMedia.TITLE, AudioMedia.DURATION, AudioMedia.DATA };

    OnScanCompletedListener mScanCompletedListener = null;

    @Override
    public boolean onCreate() {
        if(Build.VERSION.SDK_INT>7){
            mScanCompletedListener = new OnScanCompletedListener() {
                @Override
                public void onScanCompleted(String path, Uri uri) {
                    Logger.d("Scanned " + path + ":");
                    Logger.d("-> uri=" + uri);
                    getContext().getContentResolver().notifyChange(ARTIST_CONTENT_URI,
                            null);
                    getContext().getContentResolver().notifyChange(ALBUM_CONTENT_URI,
                            null);
                    getContext().getContentResolver().notifyChange(MEDIA_CONTENT_URI,
                            null);
                }
            };
        }
        
        mPreference = PreferenceManager
                .getDefaultSharedPreferences(getContext()
                        .getApplicationContext());
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/media", CODE_MEDIA);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/media/#", CODE_MEDIA_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/albums", CODE_ALBUMS);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/albums/#", CODE_ALBUMS_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/artist", CODE_ARTIST);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/artist/#", CODE_ARTIST_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/albumart", AUDIO_ALBUMART);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/albumart/#",
                AUDIO_ALBUMART_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/media/#/albumart",
                AUDIO_ALBUMART_FILE_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/playlist", CODE_PLAYLIST);
        uriMatcher
                .addURI(MEDIA_AUTHORITY, "audio/playlist/#", CODE_PLAYLIST_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/playlistmember",
                CODE_PLAYLISTMEMBER);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/playlistmember/#",
                CODE_PLAYLISTMEMBER_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/favorite", CODE_FAVORITE);
        uriMatcher
                .addURI(MEDIA_AUTHORITY, "audio/favorite/#", CODE_FAVORITE_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/file", CODE_FILE);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/file/#", CODE_FILE_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "video/media", CODE_VIDEO);
        uriMatcher.addURI(MEDIA_AUTHORITY, "video/media/#", CODE_VIDEO_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "order/audio", CODE_ORDER_AUDIO);
        uriMatcher
                .addURI(MEDIA_AUTHORITY, "order/audio/#", CODE_ORDER_AUDIO_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/genres", CODE_GENRES);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/genres/#", CODE_GENRES_ID);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/genresmember",
                CODE_GENRESMEMBER);
        uriMatcher.addURI(MEDIA_AUTHORITY, "audio/genresmember/#",
                CODE_GENRESMEMBER_ID);

        helper = new JustPlayerDatabaseHelper(getContext());

        return true;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
        case CODE_FAVORITE: {
            return favoriteDelete(uri, selection, selectionArgs);
        }
        case CODE_ORDER_AUDIO: {
            return orderAudioDelete(uri, selection, selectionArgs);
        }
        case CODE_PLAYLIST: {
            return playlistDelete(uri, selection, selectionArgs);
        }
        case CODE_PLAYLIST_ID: {
            return playlistmemberDelete(uri, selection, selectionArgs);
        }
        }
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int match = uriMatcher.match(uri);
        switch (match) {
        case CODE_FAVORITE: {
            return favoriteInsert(uri, values);
        }
        case CODE_ORDER_AUDIO: {
            return orderAudioInsert(uri, values);
        }
        case CODE_PLAYLIST: {
            return playlistInsert(uri, values);
        }
        case CODE_PLAYLIST_ID: {
            return playlistmemberInsert(uri, values);
        }
        }
        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        int match = uriMatcher.match(uri);
        switch (match) {
        case CODE_MEDIA: {
            return mediaQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_MEDIA_ID: {
            return mediaIdQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_ALBUMS: {
            return albumQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_ARTIST: {
            return artistQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_PLAYLIST: {
            return playlistQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_PLAYLIST_ID: {
            return playlistmemberQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_GENRES: {
            return genresQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_GENRESMEMBER: {
            return genresmemberQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_FAVORITE: {
            return favoriteQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_FAVORITE_ID: {
            return favoriteIdQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_VIDEO: {
            return videoQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_VIDEO_ID: {
            return videoIdQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_ORDER_AUDIO: {
            return orderAudioQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_ORDER_AUDIO_ID: {
            return orderAudioIdQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_FILE: {
            return fileQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_FILE_ID: {
            return fileIdQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        }
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
        case CODE_MEDIA_ID: {
            return mediaIdUpdate(uri, values, selection, selectionArgs);
        }
        case CODE_ARTIST_ID: {
            return artistIdUpdate(uri, values, selection, selectionArgs);
        }
        case CODE_ALBUMS_ID: {
            return albumIdUpdate(uri, values, selection, selectionArgs);
        }
        case CODE_ORDER_AUDIO: {
            return orderAudioUpdate(uri, values, selection, selectionArgs);
        }
        case CODE_ORDER_AUDIO_ID: {
            return orderAudioIdUpdate(uri, values, selection, selectionArgs);
        }
        case CODE_FAVORITE: {
            return favoriteUpdate(uri, values, selection, selectionArgs);
        }
        case CODE_FAVORITE_ID: {
            return favoriteIdUpdate(uri, values, selection, selectionArgs);
        }
        }
        return 0;
    }

    public int favoriteDelete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.beginTransaction();
            int ret = db.delete(TableConsts.TBNAME_FAVORITE, selection,
                    selectionArgs);
            db.endTransaction();
            return ret;
        } finally {
            db.endTransaction();
        }
    }

    public int playlistDelete(Uri uri, String selection, String[] selectionArgs) {
        return getContext().getContentResolver().delete(
                MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, selection,
                selectionArgs);
    }

    public int playlistmemberDelete(Uri uri, String selection,
            String[] selectionArgs) {
        long playlist_id = ContentUris.parseId(uri);
        Uri targeturi = MediaStore.Audio.Playlists.Members.getContentUri(
                "external", playlist_id);
        return getContext().getContentResolver().delete(targeturi,
                selection, selectionArgs);
    }

    public int orderAudioDelete(Uri uri, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.beginTransaction();
            int ret = db.delete(TableConsts.TBNAME_PLAYBACK, selection,
                    selectionArgs);
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }

    public Uri orderAudioInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_PLAYBACK, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(
                    MediaConsts.PLAYBACK_CONTENT_URI, id);
        } finally {
            db.endTransaction();
        }
    }

    public Uri favoriteInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = null;
        try {
            db = helper.getWritableDatabase();
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_FAVORITE, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(MediaConsts.FAVORITE_CONTENT_URI,
                    id);
        } finally {
            db.endTransaction();
        }
    }

    public Uri playlistInsert(Uri uri, ContentValues values) {
        values.remove(AudioPlaylist.PLAYLIST_KEY);
        // values.remove(TableConsts.PLAYLIST_DEL_FLG);
        return getContext().getContentResolver().insert(
                MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, values);
    }

    public Uri playlistmemberInsert(Uri uri, ContentValues values) {
        long id = ContentUris.parseId(uri);
        Uri targeturi = MediaStore.Audio.Playlists.Members.getContentUri(
                "external", id);
        return getContext().getContentResolver().insert(targeturi, values);
    }

    private String getAlbumKey(long id) {
        Cursor cur = getContext().getContentResolver().query(
                ContentUris.withAppendedId(
                        MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, id),
                new String[] { AudioAlbum.ALBUM_KEY }, null, null, null);
        if (cur != null) {
            try {
                if (cur.moveToFirst()) {
                    return cur.getString(cur
                            .getColumnIndex(AudioAlbum.ALBUM_KEY));
                }
            } finally {
                if (cur != null) {
                    cur.close();
                }
            }
        }
        return null;
    }

    private String getArtistName(long id) {
        Cursor cur = getContext().getContentResolver().query(
                ContentUris.withAppendedId(
                        MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI, id),
                new String[] { AudioArtist.ARTIST }, null, null, null);
        if (cur != null) {
            try {
                if (cur.moveToFirst()) {
                    return cur
                            .getString(cur.getColumnIndex(AudioArtist.ARTIST));
                }
            } finally {
                if (cur != null) {
                    cur.close();
                }
            }
        }
        return null;
    }

    private int orderAudioUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = null;
        try {
            db = helper.getWritableDatabase();
            db.beginTransaction();
            int ret = db.update(TableConsts.TBNAME_PLAYBACK, values, selection, selectionArgs);
            Logger.d("orderAudioUpdate="+ret);
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }
    
    private int orderAudioIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        long id = ContentUris.parseId(uri);
        SQLiteDatabase db = null;
        try {
            db = helper.getWritableDatabase();
            db.beginTransaction();
            int ret = db.update(TableConsts.TBNAME_PLAYBACK, values, BaseColumns._ID + " = ?", new String[]{Long.toString(id)});
            Logger.d("orderAudioIdUpdate="+ret+" id = "+ id);
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }
    
    private int mediaIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            long id = ContentUris.parseId(uri);
            if (id >= 0) {
                String enc = values.getAsString(AudioMedia.ENCODING);
                Cursor cur = getContext()
                        .getContentResolver()
                        .query(ContentUris
                                .withAppendedId(
                                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                                        id),
                                new String[] { AudioMedia.DATA,
                                        AudioMedia.MIME_TYPE }, null, null,
                                null);
                setId3Tag(cur, enc);
                return 1;
            }
        }
        return 0;
    }

    private int artistIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            long id = ContentUris.parseId(uri);
            if (id >= 0) {
                String artistName = getArtistName(id);
                if (artistName != null) {
                    String enc = values.getAsString(AudioMedia.ENCODING);
                    Cursor cur = getContext().getContentResolver().query(
                            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                            new String[] { AudioMedia.DATA,
                                    AudioMedia.MIME_TYPE },
                            AudioMedia.ARTIST + " = ?",
                            new String[] { artistName }, null);

                    setId3Tag(cur, enc);
                }
            }
        }
        return 0;
    }

    private int albumIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            long id = ContentUris.parseId(uri);
            if (id >= 0) {
                String albumkey = getAlbumKey(id);
                if (albumkey != null) {
                    String enc = values.getAsString(AudioMedia.ENCODING);
                    Cursor cur = getContext().getContentResolver().query(
                            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                            new String[] { AudioMedia.DATA,
                                    AudioMedia.MIME_TYPE },
                            AudioMedia.ALBUM_KEY + " = ?",
                            new String[] { albumkey }, null);

                    setId3Tag(cur, enc);
                }
            }
        }
        return 0;
    }

    @TargetApi(8)
    private boolean setId3Tag(Cursor cur, String enc) {
        if (cur != null) {
            try {
                if (cur.moveToFirst()) {
                    ArrayList<String> files = new ArrayList<String>();
                    ArrayList<String> mimefiles = new ArrayList<String>();
                    int colnum1 = cur.getColumnIndex(AudioMedia.DATA);
                    int colnum2 = cur.getColumnIndex(AudioMedia.MIME_TYPE);
                    do {
                        String fname = cur.getString(colnum1);
                        String mimetype = cur.getString(colnum2);
                        File mp3file = new File(fname);
                        if (mp3file.exists()) {
                            // update file
                            files.add(fname);
                            mimefiles.add(mimetype);
                        }
                    } while (cur.moveToNext());
                    if (files.size() > 0) {
                        String[] path_files = (String[]) files
                                .toArray(new String[files.size()]);
                        ID3Helper conv = new ID3Helper();
                        conv.isDebug = true;
                        conv.isInfo = true;
                        // conv.isTest = true;
                        conv.conv(path_files, enc);
                        if(Build.VERSION.SDK_INT>7){
                            MediaScannerConnection.scanFile(getContext(),
                                    (String[]) files.toArray(new String[files
                                            .size()]), (String[]) mimefiles
                                            .toArray(new String[mimefiles.size()]),
                                    mScanCompletedListener);
                        }
                        return true;
                    }
                }
            } finally {
                cur.close();
            }
        }
        return false;
    }

    public int favoriteUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = null;
        try {
            db = helper.getWritableDatabase();
            db.beginTransaction();
            int ret = db.update(TableConsts.TBNAME_FAVORITE, values, selection,
                    selectionArgs);
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }

    public int favoriteIdUpdate(Uri uri, ContentValues values,
            String selection, String[] selectionArgs) {
        SQLiteDatabase db = null;
        try {
            long id = ContentUris.parseId(uri);
            db = helper.getWritableDatabase();
            db.beginTransaction();
            int ret = db.update(TableConsts.TBNAME_FAVORITE, values,
                    BaseColumns._ID + " = ?",
                    new String[] { Long.toString(id) });
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }
    
    @Override
    public AssetFileDescriptor openAssetFile(Uri uri, String mode) throws FileNotFoundException {
        String u = uri.toString().replaceFirst(MEDIA_AUTHORITY, "media/external");
        return getContext().getContentResolver().openAssetFileDescriptor(Uri.parse(u), mode);
    }

    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        String u = uri.toString().replaceFirst(MEDIA_AUTHORITY, "media/external");
        return getContext().getContentResolver().openFileDescriptor(Uri.parse(u), mode);
    }

    public Cursor mediaQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        if (projection == null) {
            projection = media_fetchcols;
        }
        if(selection == null){
            selection = MediaStore.Audio.Media.IS_MUSIC + " = 1";
        }
        return getContext().getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor mediaIdQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        long id = ContentUris.parseId(uri);
        if (projection == null) {
            projection = media_fetchcols;
        }
        return getContext().getContentResolver().query(
                ContentUris.withAppendedId(
                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id),
                projection, selection, selectionArgs, sortOrder);
    }

    public Cursor albumQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        return getContext().getContentResolver().query(
                MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor artistQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        return getContext().getContentResolver().query(
                MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor playlistQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        return getContext().getContentResolver().query(
                MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor playlistmemberQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        long id = ContentUris.parseId(uri);
        return getContext().getContentResolver().query(
                MediaStore.Audio.Playlists.Members.getContentUri("external",
                        id), projection,
                MediaStore.Audio.Media.IS_MUSIC + " != 0 ", null, sortOrder);
    }

    public Cursor genresQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        return getContext().getContentResolver().query(
                MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor genresmemberQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        long genreskey = Long.parseLong(selectionArgs[0]);
        return getContext().getContentResolver().query(
                MediaStore.Audio.Genres.Members.getContentUri("external",
                        genreskey), projection,
                MediaStore.Audio.Media.IS_MUSIC + " != 0 ", null, sortOrder);
    }

    private Cursor orderAudioQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = helper.getReadableDatabase();
        return db.query(TableConsts.TBNAME_PLAYBACK, projection, selection,
                selectionArgs, null, null, sortOrder);
    }
    
    public Cursor orderAudioIdQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        long id = ContentUris.parseId(uri);
        SQLiteDatabase db = helper.getReadableDatabase();
        return db.query(TableConsts.TBNAME_PLAYBACK, projection, BaseColumns._ID + " = ?", new String[]{Long.toString(id)}, null, null, null);
    }

    public Cursor favoriteQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        try {
            db = helper.getReadableDatabase();
            return db.query(TableConsts.TBNAME_FAVORITE, projection, selection,
                    selectionArgs, null, null, sortOrder);
        } finally {
        }
    }

    public Cursor favoriteIdQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        try {
            long id = ContentUris.parseId(uri);
            db = helper.getReadableDatabase();
            return db.query(TableConsts.TBNAME_FAVORITE, projection,
                    BaseColumns._ID + " = ?",
                    new String[] { Long.toString(id) }, null, null, sortOrder);
        } finally {
        }
    }

    public Cursor fileQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        String find_path = mPreference.getString(SystemConsts.PREF_ROOTPATH,
                Environment.getExternalStorageDirectory().getPath());
        if (selection != null && selection.indexOf(FileMedia.DATA) != -1) {
            String[] sel = selection.split("=");
            String path = null;
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(FileMedia.DATA) != -1) {
                    path = selectionArgs[i];
                    break;
                }
            }
            if (path != null && !path.equals(find_path)) {
                if (!path.equals("..")) {
                    find_path = path;
                } else {
                    find_path = new File(find_path).getParent();
                    // 親がない場合
                    if (find_path == null || find_path.length() == 0) {
                        find_path = "/";// Environment.getExternalStorageDirectory().getPath();
                    }
                }
                Editor editor = mPreference.edit();
                editor.putString(SystemConsts.PREF_ROOTPATH, find_path);
                editor.commit();
            }
        }

        MatrixCursor cursor = new MatrixCursor(new String[] { FileMedia._ID,
                FileMedia.TITLE, FileMedia.TYPE, FileMedia.DATE_MODIFIED,
                FileMedia.SIZE, FileMedia.DATA });

        if (!find_path.equals("/")) {
            cursor.addRow(new Object[] { "..".hashCode(), "..",
                    FileType.FOLDER, 0, 0, ".." });
        }

        File dir = new File(find_path);
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files != null) {
                for (File file : files) {
                    String type = getType(file);
                    if (type != null) {
                        cursor.addRow(new Object[] { file.getPath().hashCode(),
                                file.getName(), type, file.lastModified(),
                                file.length(), file.getPath() });
                    }
                }
            }
        } else {
            // Folderじゃないということは、ZIP
            ZipFile zip = null;
            try {
                zip = new ZipFile(dir);
                for (Enumeration<? extends ZipEntry> e = zip.entries(); e
                        .hasMoreElements();) {
                    ZipEntry ze = e.nextElement();
                    String type = getType(ze);
                    if (type != null && type.equals(FileType.AUDIO)) {
                        String name = ze.getName().replaceAll("(.+?)\\/", "");
                        String path = "zip://" + dir + "/" + ze.getName();
                        cursor.addRow(new Object[] { path.hashCode(), name,
                                type, ze.getTime(), ze.getSize(), path });
                    }
                }
            } catch (ZipException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (zip != null) {
                    try {
                        zip.close();
                    } catch (IOException e) {
                    }
                }
            }

        }
        return cursor;
    }

    public Cursor fileIdQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        MatrixCursor ret_cursor = new MatrixCursor(MEDIA_PROJECTION);
        if (selection != null && selection.indexOf(FileMedia.DATA) != -1) {
            String[] sel = selection.split("=");
            String path = null;
            String type = null;
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(FileMedia.DATA) != -1) {
                    path = selectionArgs[i];
                } else if (sel[i].indexOf(FileMedia.TYPE) != -1) {
                    type = selectionArgs[i];
                }
            }
            if (path != null && !path.equals("..")) {
                if (FileType.AUDIO.equals(type)) {
                    Cursor media_cursor = null;
                    try {
                        media_cursor = getContext().getContentResolver().query(
                                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                                MEDIA_PROJECTION, AudioMedia.DATA + " = ?",
                                new String[] { path }, null);
                        if (media_cursor != null && media_cursor.moveToFirst()) {
                            long media_id = media_cursor.getLong(media_cursor
                                    .getColumnIndex(AudioMedia._ID));
                            String album = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.ALBUM));
                            String album_key = media_cursor
                                    .getString(media_cursor
                                            .getColumnIndex(AudioMedia.ALBUM_KEY));
                            String artist = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.ARTIST));
                            String title = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.TITLE));
                            long duration = media_cursor.getLong(media_cursor
                                    .getColumnIndex(AudioMedia.DURATION));
                            String data = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.DATA));
                            ret_cursor.addRow(new Object[] { media_id, album,
                                    album_key, artist, title, duration, data });
                        } else {
                            // ID3タグ読み取りをつかって情報を作成し返却する
                            if (path.startsWith("zip://")) {
                                String fname = path.replaceAll("(.+?)\\.zip/",
                                        "");
                                // String zipfname =
                                // path.substring("zip://".length(),
                                // path.length()-fname.length()-1);

                                String[] split_name = fname.split("/");
                                if (split_name != null && split_name.length > 1) {
                                    long media_id = Funcs.makeSubstansId(path);
                                    String album = split_name[0];
                                    String album_key = null;
                                    String artist = null;
                                    String title = split_name[1];
                                    long duration = 0;// metadata.get("xmpDM:album");
                                    String data = path;

                                    ret_cursor.addRow(new Object[] { media_id,
                                            album, album_key, artist, title,
                                            duration, data });
                                }
                            } else {
                                Map<String, Object> ret = getMMS(path);
                                if(ret != null){
                                    ret_cursor.addRow(new Object[] { 
                                            ret.get("media_id"),
                                            ret.get("album"), 
                                            ret.get("album_key"), 
                                            ret.get("artist"), 
                                            ret.get("title"),
                                            ret.get("duration"),
                                            path });
                                }
                                else{
                                    File file =  new File(path);
                                    long media_id = Funcs.makeSubstansId(path);
                                    String album = file.getParentFile().getName();
                                    String album_key = null;
                                    String artist = null;
                                    String title = file.getName();
                                    long duration = 0;// metadata.get("xmpDM:album");
                                    String data = path;

                                    ret_cursor.addRow(new Object[] { media_id,
                                            album, album_key, artist, title,
                                            duration, data });
                                }
                            }
                        }
                    } finally {
                        if (media_cursor != null) {
                            media_cursor.close();
                        }
                    }
                } else if (FileType.VIDEO.equals(type)) {

                } else {
                    // ZIP
                    // ID3タグ読み取りをつかって情報を作成し返却する
                    String fname = path.replaceAll("(.+?)\\.zip/", "");
                    // String zipfname = path.substring("zip://".length(),
                    // path.length()-fname.length()-1);
                    String[] split_name = fname.split("/");
                    if (split_name != null && split_name.length > 1) {
                        long media_id = Funcs.makeSubstansId(path);
                        String album = split_name[0];
                        String album_key = null;
                        String artist = null;
                        String title = split_name[1];
                        long duration = 0;// metadata.get("xmpDM:album");
                        String data = path;

                        ret_cursor.addRow(new Object[] { media_id, album,
                                album_key, artist, title, duration, data });
                    }
                }
            }
        }

        return ret_cursor;
    }
    
    @TargetApi(10)
    private Map<String, Object> getMMS(String path){
        if(Build.VERSION.SDK_INT>7){
            try{
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(getContext(), Uri.parse(path));
                long media_id = Funcs.makeSubstansId(path);
                String album = mmr
                        .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
                String album_key = "";
                String artist = mmr
                        .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                String title = mmr
                        .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                long duration = Long
                        .parseLong(mmr
                                .extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
                HashMap<String, Object> ret = new HashMap<String, Object>();
                
                ret.put("media_id", media_id);
                ret.put("album", album);
                ret.put("album_key", album_key);
                ret.put("title", title);
                ret.put("duration", duration);
                
                return ret;
            }
            catch(Exception e){
                Logger.e("Can not parse id3.", e);
            }
        }
        return null;
    }

    private String getType(File file) {
        if (file.isDirectory()) {
            return FileType.FOLDER;
        }
        MediaFileType type = MediaFile.getFileType(file.getName());
        if (type != null) {
            if (MediaFile.isAudioFileType(type.fileType)) {
                return FileType.AUDIO;
            } else if (MediaFile.FILE_TYPE_ZIP == type.fileType) {
                return FileType.ZIP;
            } else if (MediaFile.isVideoFileType(type.fileType)) {
                return FileType.VIDEO;
            }
        }
        return null;
    }

    private String getType(ZipEntry ze) {
        if (ze.isDirectory()) {
            return "folder";
        }
        MediaFileType type = MediaFile.getFileType(ze.getName());
        if (type != null) {
            if (MediaFile.isAudioFileType(type.fileType)) {
                return "audio";
            } else if (MediaFile.FILE_TYPE_ZIP == type.fileType) {
                return "zip";
            }
        }
        return null;
    }

    public Cursor videoQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        return getContext().getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor videoIdQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        long id = ContentUris.parseId(uri);
        return getContext().getContentResolver().query(
                ContentUris.withAppendedId(
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id),
                projection, selection, selectionArgs, sortOrder);
    }

}
