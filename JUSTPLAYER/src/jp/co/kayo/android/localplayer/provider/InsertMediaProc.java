package jp.co.kayo.android.localplayer.provider;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.util.Funcs;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class InsertMediaProc implements Runnable {
    SQLiteDatabase db;
    String tbname;
    String[] projection;
    ArrayList<Object> values;

    public InsertMediaProc(SQLiteDatabase db, String tbname,
            String[] projection, ArrayList<Object> values) {

        this.db = db;
        this.tbname = tbname;
        this.projection = projection;
        this.values = values;
    }

    @Override
    public void run() {

        ContentValues val = new ContentValues(projection.length);
        for (int i = 0; i < projection.length; i++) {
            Object o = values.get(i);
            if (projection[i].equals(BaseColumns._ID)) {

            } else if (projection[i].equals(AudioMedia.MEDIA_KEY)) {
                val.put(projection[i], (String) o);
            } else if (projection[i].equals(AudioMedia.TITLE)) {
                val.put(projection[i], (String) o);
            } else if (projection[i].equals(AudioMedia.TITLE_KEY)) {
                val.put(projection[i], (String) o);
            } else if (projection[i].equals(AudioMedia.DURATION)) {
                val.put(projection[i], (Long) o);
            } else if (projection[i].equals(AudioMedia.ARTIST)) {
                val.put(projection[i], (String) o);
            } else if (projection[i].equals(AudioMedia.ARTIST_KEY)) {
                val.put(projection[i], (String) o);
            } else if (projection[i].equals(AudioMedia.ALBUM)) {
                val.put(projection[i], (String) o);
            } else if (projection[i].equals(AudioMedia.ALBUM_KEY)) {
                val.put(projection[i], (String) o);
            }
            /*
             * else if(projection[i].equals(AudioMedia.ALBUM_ART)){
             * val.put(projection[i], (String)o); }
             */
            else if (projection[i].equals(AudioMedia.DATA)) {
                val.put(projection[i], (String) o);
            } else if (projection[i].equals(AudioMedia.TRACK)) {
                val.put(projection[i], Funcs.parseInt((String) o));
            } else if (projection[i].equals(AudioMedia.DATE_ADDED)) {
                val.put(projection[i], (Long) o);
            } else if (projection[i].equals(AudioMedia.DATE_MODIFIED)) {
                val.put(projection[i], (Long) o);
            }
        }
        val.put(TableConsts.AUDIO_DEL_FLG, 0);

        db.insert(tbname, null, val);
    }

}
