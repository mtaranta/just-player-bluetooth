
package jp.co.kayo.android.localplayer;

import java.io.ObjectInputStream.GetField;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;

import jp.co.kayo.android.localplayer.MyTabsAdapter.TabInfo;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.IProgressView;
import jp.co.kayo.android.localplayer.dialog.OpenPlaylistDialog;
import jp.co.kayo.android.localplayer.dialog.SavePlaylistDialog;
import jp.co.kayo.android.localplayer.fragment.ControlFragment;
import jp.co.kayo.android.localplayer.fragment.MainFragment;
import jp.co.kayo.android.localplayer.fragment.PlaybackListViewFragment;
import jp.co.kayo.android.localplayer.menu.DataSourceMenu;
import jp.co.kayo.android.localplayer.menu.PlaybackMenu;
import jp.co.kayo.android.localplayer.menu.ShareMenu;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.LocalSuggestionProvider;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.IMediaPlayerServiceCallback;
import jp.co.kayo.android.localplayer.service.MediaPlayerService;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.StrictHelper;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.NfcHelper.BeamMessage;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.WindowManager.LayoutParams;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

@TargetApi(8)
public class MainActivity2 extends BaseActivity implements OnQueryTextListener {
    SharedPreferences mPref;
    ViewPager mViewPager;
    private BillingService mBillingService = null;
    DungeonsPurchaseObserver mDungeonsPurchaseObserver;
    private ViewCache mViewCache;
    public MyTabsAdapter mTabsAdapter;
    Handler mHandler = new MyHandler(this);
    private Intent oneIntent;
    private SearchView mSearchView;

    private static class MyHandler extends Handler {
        WeakReference<MainActivity2> ref;

        MyHandler(MainActivity2 r) {
            ref = new WeakReference<MainActivity2>(r);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity2 main = ref.get();
            if (main != null) {
                if (msg.what == SystemConsts.EVT_DSCHANFED) {
                    try {
                        IMediaPlayerService binder = main.getBinder();
                        if (binder != null) {
                            binder.setContentsKey(null);
                            binder.stop();
                            //binder.clear();
                            binder.reload();
                        }
                    } catch (RemoteException e) {
                    }
                    
                    if(main.getSupportFragmentManager().getBackStackEntryCount()>0){
                        main.getSupportFragmentManager().popBackStack();
                        Funcs.setResumeReloadFlag(main.mPref, true);
                    }else{
                        for (int i = 0; i < main.mTabsAdapter.getCount(); i++) {
                            ContentManager mgr = (ContentManager) main.mTabsAdapter.getFragment(i);
                            if (mgr != null) {
                                mgr.reload();
                            }
                        }   
                    }
                }
                else if (msg.what == SystemConsts.EVT_SELECT_PLAYBACK_CLEAR) {
                    IMediaPlayerService binder = main.getBinder();
                    if (binder != null) {
                        try {
                            binder.setContentsKey(null);
                            binder.clear();
                        } catch (RemoteException e) {
                        }
                    }
                }
                else if (msg.what == SystemConsts.EVT_SELECT_PLAYBACK_SAVE) {
                    IMediaPlayerService binder = main.getBinder();
                    if (binder != null) {
                        long[] ids;
                        try {
                            ids = binder.getList();
                            if (ids != null && ids.length > 0) {
                                SavePlaylistDialog dlg = new SavePlaylistDialog();
                                Bundle b = new Bundle();
                                b.putLongArray("playlist", ids);
                                dlg.setArguments(b);
                                dlg.show(main.getSupportFragmentManager(), SystemConsts.TAG_RATING_DLG);
                            }
                        } catch (RemoteException e) {
                            Logger.e("mnu_save_playlist", e);
                        } finally {

                        }
                    }
                }
                else if (msg.what == SystemConsts.EVT_SELECT_PLAYBACK_LOAD) {
                    OpenPlaylistDialog dlg = new OpenPlaylistDialog();
                    dlg.show(main.getSupportFragmentManager(), "playlist.dlg");
                }
                else if (msg.what == SystemConsts.EVT_SELECT_VIEW) {
                    int itemid = (Integer)msg.obj;
                    MainFragment f = (MainFragment) main.mTabsAdapter.getFragment(itemid);
                    String tabname = f.selectView(main, itemid);
                    if (tabname != null) {
                        Tab tab = main.getSupportActionBar().getTabAt(0);
                        main.mTabsAdapter.setTabText(tab, tabname);
                    }
                }
                else if (msg.what == SystemConsts.EVT_UPDATE_LISTVIEW) {
                    main.mTabsAdapter.notifyDataSetChanged();
                }

            }
        }
    }

    IMediaPlayerServiceCallback.Stub mCallback = new IMediaPlayerServiceCallback.Stub() {
        ControlFragment control;

        ControlFragment getControll() {
            if (control == null) {
                FragmentManager m = getSupportFragmentManager();
                control = (ControlFragment) m
                        .findFragmentByTag(SystemConsts.TAG_CONTROL);
            }
            return control;
        }

        @Override
        public void updateView(final boolean updatelist) throws RemoteException {
            Logger.d("updateView=" + updatelist);
            // もし、コントロール部分が表示されているならリストを更新してあげて
            if (mHandler != null) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        // AlbumList,ArtistList,Song等
                        ControlFragment control = getControll();
                        // 再生中の曲の更新
                        if (mBinder != null) {
                            try {
                                mViewCache.setPosition(mBinder.getPosition(), mBinder.getMediaId(),
                                        mBinder.getPrefetchId());
                            } catch (RemoteException e) {
                            }
                        }

                        // 更新処理
                        if (control != null) {
                            control.updateView();
                        }

                        for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                            Fragment f = mTabsAdapter.getFragment(i);
                            if (f != null) {
                                Fragment f2 = getSupportFragmentManager().findFragmentById(FragmentUtils.getFragmentId(MainActivity2.this, f));
                                if(f2!=null){
                                    if (updatelist && f2 instanceof PlaybackListViewFragment) {
                                        ((PlaybackListViewFragment)f2).reload();
                                    } else {
                                        ((ContentManager)f2).changedMedia();
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }

        @Override
        public void updateList() throws RemoteException {
            // もし、再生中のフラグメントが表示されているならリストを更新してあげて
        }

        @Override
        public void onBufferingUpdate(int percent) throws RemoteException {
            // バッファリング中のプログレスバーですよぉ
            ControlFragment f = getControll();
            if (f != null) {
                f.onBufferingUpdate(percent);
            }
        }

        @Override
        public void startProgress(long max) throws RemoteException {
            long media_id = mBinder.getMediaId();
            int pos = mBinder.getPosition();
            long pref_id = mBinder.getPrefetchId();
            mViewCache.setPosition(pos, media_id, pref_id);
            Fragment current = getCurrentFragment();
            if (current != null && current instanceof IProgressView) {
                ((IProgressView) current).startProgress(max);
            }
        }

        @Override
        public void stopProgress() throws RemoteException {
            Fragment current = getCurrentFragment();
            if (current != null && current instanceof IProgressView) {
                ((IProgressView) current).stopProgress();
            }
        }

        @Override
        public void progress(long pos, long max) throws RemoteException {
            Fragment current = getCurrentFragment();
            if (current != null && current instanceof IProgressView) {
                ((IProgressView) current).progress(pos, max);
            }
        }

        @Override
        public void close() throws RemoteException {
            MainActivity2.this.finish();
        }
    };

    ContentObserver mAlbumContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
        }
    };

    ContentObserver mArtistContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
        }
    };

    ContentObserver mMediaContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SystemConsts.REQUEST_TAGEDIT) {
            // 変更を通知
            for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                ContentManager f = (ContentManager) mTabsAdapter.getFragment(i);
                if (f != null) {
                    f.reload();
                }
            }
        } else if (requestCode == SystemConsts.REQUEST_DSCHANGED) {
            mHandler.sendEmptyMessage(SystemConsts.EVT_DSCHANFED);
        } else if (requestCode == SystemConsts.REQUEST_ALBUMART) {
            if (data != null) {
                String album_key = data.getStringExtra("album_key");
                if (album_key != null) {
                    ContentsUtils.reloadAlbumArt(this, album_key);
                    // 変更を通知
                    for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                        ContentManager f = (ContentManager) mTabsAdapter.getFragment(i);
                        if (f != null) {
                            f.reload();
                        }
                    }
                }
            }
        }
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        StrictHelper.registStrictMode();
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);

        mPref = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.main_tab);

        hideProgressBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(
                getSupportFragmentManager().getBackStackEntryCount() > 0);
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        getSupportActionBar().setSubtitle(getString(R.string.hello));

        getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // SDCardChec
        Funcs.checkSDCard(this);

        // Audio Volume
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        // ViewCache
        mViewCache = (ViewCache) getSupportFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);

        // setup
        mViewPager = (ViewPager) findViewById(R.id.pager);

        mTabsAdapter = new MyTabsAdapter(this, getSupportActionBar(), mViewPager);

        int tabcount = Funcs.getInt(mPref.getString("key.tabcount", "2"));
        for(int i=0; i<tabcount; i++){
            ActionBar.Tab maintab = getSupportActionBar().newTab().setText(getString(R.string.lb_tab_blank));
            Bundle bundle1 = new Bundle();
            bundle1.putInt("tabposition", i);
            bundle1.putString("tagname", "tab"+(i+1));
            mTabsAdapter.addTab(maintab, MainFragment.class, bundle1);
        }

        getSupportFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(
                        getSupportFragmentManager().getBackStackEntryCount() > 0);

                try{
                    boolean reload = Funcs.isResumeReloadFlag(mPref);
                    for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                        Tab tab = getSupportActionBar().getTabAt(i);
                        ContentManager mgr = (ContentManager) mTabsAdapter.getFragment(i);
                        if (mgr != null) {
                            String tabname = mgr.getName(MainActivity2.this);
                            mTabsAdapter.setTabText(tab, tabname);
                            if (reload) {
                                mgr.reload();
                            }
                        }
                    }
                }
                finally{
                    Funcs.setResumeReloadFlag(mPref, false);
                }
            }
        });

        // ContentResolver
        getContentResolver().registerContentObserver(
                MediaConsts.ALBUM_CONTENT_URI, true, mAlbumContentObserver);
        getContentResolver().registerContentObserver(
                MediaConsts.ARTIST_CONTENT_URI, true, mArtistContentObserver);
        getContentResolver().registerContentObserver(
                MediaConsts.MEDIA_CONTENT_URI, true, mMediaContentObserver);

        Intent intent = getIntent();
        if (intent == null
                || intent.getAction() == null
                || !intent.getAction()
                        .equals(SystemConsts.MAIN_ACITON_SHOWHOMW)) {
            // 初回起動か確認
            boolean isFirst = mPref.getBoolean("is_first", true);
            if (isFirst) {
                Editor editor = mPref.edit();
                editor.putBoolean("is_first", false);
                editor.commit();
                // まず、既にアプリを購入済みか確認する
                mBillingService = new BillingService();
                mDungeonsPurchaseObserver = new DungeonsPurchaseObserver(this,
                        mBillingService, new Handler());
                ResponseHandler.register(mDungeonsPurchaseObserver);
                mBillingService.setContext(this);
                mBillingService.restoreTransactions();

            }
        }

        if (savedInstanceState != null) {
            int index = savedInstanceState.getInt("index");
            selectTab(index);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        if (procIntent(intent) == false) {
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        procIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            doSearchWithIntent(intent);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            if (intent.getFlags() == Intent.FLAG_ACTIVITY_NEW_TASK) {
                doSearchWithIntent(intent);
            }
        }
    }

    @Override
    public void onServiceConnected(IMediaPlayerService binder) {
        if (oneIntent != null) {
            revieveNdef(oneIntent);
        }
        for (int i = 0; i < mTabsAdapter.getCount(); i++) {
            ContentManager f = (ContentManager) mTabsAdapter.getFragment(i);
            if (f != null) {
                if (f instanceof PlaybackListViewFragment) {
                    f.reload();
                }
                else {
                    f.changedMedia();
                }
            }
        }
    }

    public ArrayList<Tab> getPlaybackTab() {
        ArrayList<Tab> tabs = new ArrayList<Tab>();
        for (int i = 0; i < getSupportActionBar().getTabCount(); i++) {
            Tab tab = getSupportActionBar().getTabAt(i);
            TabInfo info = (TabInfo) tab.getTag();
            if (info != null && info.tag.equals(getString(R.string.lb_tab_order))) {
                tabs.add(tab);
            }
        }
        return tabs;
    }

    private boolean procIntent(Intent intent) {
        if (intent != null) {
            if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
                if (revieveNdef(intent)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean revieveNdef(Intent intent) {
        if (getNFCHelper() != null) {
            IMediaPlayerService binder = getBinder();
            if (binder != null) {
                oneIntent = null;
                ArrayList<BeamMessage> list = getNFCHelper()
                        .getNdefMessage(intent);
                if (list != null) {
                    try {
                        String cntkey = SystemConsts.CONTENTSKEY_PLAYLIST
                                + System.currentTimeMillis();
                        try {
                            binder.setContentsKey(cntkey);
                            binder.lockUpdateToPlay();
                            binder.clearcut();
                            for (int i = 0; i < list.size(); i++) {
                                BeamMessage msg = list.get(i);
                                try {
                                    binder.addMediaD(0, msg.duration,
                                            msg.title, msg.album,
                                            msg.artist, msg.url);
                                } catch (RemoteException e) {
                                }
                            }
                        } finally {
                            binder.play();
                        }
                    } catch (RemoteException e) {
                    }
                    return true;
                }
            }
        }
        oneIntent = intent;
        return false;
    }

    public int selectedTabPosition(){
        Tab tab = getSupportActionBar().getSelectedTab();
        if(tab != null){
            int position = tab.getPosition();
            return position;
        }
        return -1;
    }
    
    public void doSearchWithIntent(Intent intent) {
        String queryString = intent.getStringExtra(SearchManager.QUERY);
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                LocalSuggestionProvider.AUTHORITY, LocalSuggestionProvider.DATABASE_MODE_QUERIES);
        suggestions.saveRecentQuery(queryString, null);
        int position = selectedTabPosition();
        if(position != -1){
            MainFragment f = (MainFragment) mTabsAdapter.getFragment(position);
            if (f != null) {
                f.doSearchQuery(queryString);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("index", getSupportActionBar()
                .getSelectedNavigationIndex());
    }

    @Override
    protected void onResume() {
        super.onResume();
        // 音楽サービスとの接続
        if (mBinder == null) {
            Intent service = new Intent(this, MediaPlayerService.class);
            bindService(service, mConnection, 0);
            startService(service);
        }
        for (int i = 0; i < mTabsAdapter.getCount(); i++) {
            MainFragment f = (MainFragment)mTabsAdapter.getFragment(i);
            if(f!=null){
                Tab tab = getSupportActionBar().getTabAt(i);
                String tabname = f.getName(this);
                mTabsAdapter.setTabText(tab, tabname);
            }
        }
    }

    @Override
    protected void onPause() {
        if(mBinder != null){
            try {
                mBinder.unregisterCallback(mCallback);
                mBinder = null;
            } catch (RemoteException e) {
            }
            unbindService(mConnection);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {

        getContentResolver().unregisterContentObserver(mAlbumContentObserver);
        getContentResolver().unregisterContentObserver(mArtistContentObserver);
        getContentResolver().unregisterContentObserver(mMediaContentObserver);

        if (mDungeonsPurchaseObserver != null) {
            ResponseHandler.unregister(mDungeonsPurchaseObserver);
        }
        if (mBillingService != null) {
            mBillingService.unbind();
        }

        ViewCache.clearImage();

        super.onDestroy();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                Fragment fragment = getCurrentFragment();
                if (fragment != null) {
                    if (fragment instanceof ContextMenuFragment) {
                        ContextMenuFragment f = (ContextMenuFragment) fragment;
                        if (f.onBackPressed()) {
                            return true;
                        }
                    }
                }

                SharedPreferences pref = PreferenceManager
                        .getDefaultSharedPreferences(this);
                boolean exitcheck = pref.getBoolean("key.exitcheck", false);
                if (exitcheck) {
                    // ダイアログの表示
                    AlertDialog.Builder ad = new AlertDialog.Builder(this);
                    ad.setMessage(getString(R.string.alert_exit_msg));
                    ad.setPositiveButton(getString(R.string.lb_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    // OKならActivity終了
                                    finish();
                                }
                            });
                    ad.setNegativeButton(getString(R.string.lb_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                }
                            });
                    ad.create();
                    ad.show();
                    return true;
                } else {
                    return super.dispatchKeyEvent(event);
                }
            }
        }
        return super.dispatchKeyEvent(event);
    }

    MenuItem mSearchItem;

    private void setupSearchView(MenuItem searchItem) {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        mSearchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Logger.d("onQueryTextSubmit");

        String queryString = newText;
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                LocalSuggestionProvider.AUTHORITY, LocalSuggestionProvider.DATABASE_MODE_QUERIES);
        suggestions.saveRecentQuery(queryString, null);

        int position = selectedTabPosition();
        if(position != -1){
            MainFragment f = (MainFragment) mTabsAdapter.getFragment(position);
            if (f != null) {
                f.doSearchQuery(queryString);
            }
        }

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Logger.d("onQueryTextSubmit");
        mSearchItem.collapseActionView();
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.main_menu_items, menu);

        mSearchItem = menu.findItem(R.id.mnu_search);
        mSearchView = (SearchView) mSearchItem.getActionView();
        if (Build.VERSION.SDK_INT >= 8) {
            setupSearchView(mSearchItem);
        }
        
        MenuItem mnu_playback_tab = menu.findItem(R.id.mnu_playback_tab);
        if(Build.VERSION.SDK_INT <= 10){
            mnu_playback_tab.getSubMenu().clear();
        }
        
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu == null) {
            return super.onPrepareOptionsMenu(menu);
        }

        MenuItem mnu_ds = menu.findItem(R.id.mnu_ds);
        DataSourceMenu ds_provider = (DataSourceMenu) mnu_ds.getActionProvider();
        if (ds_provider != null) {
            ds_provider.setActivity(this, mHandler);
        }

        MenuItem mnu_share = menu.findItem(R.id.mnu_share);
        ShareMenu share_provider = (ShareMenu) mnu_share.getActionProvider();
        if (share_provider != null) {
            share_provider.setBinder(getBinder());
        }
        
        MenuItem mnu_playback_tab = menu.findItem(R.id.mnu_playback_tab);
        PlaybackMenu playback_provider = (PlaybackMenu) mnu_playback_tab.getActionProvider();
        if (playback_provider != null) {
            playback_provider.setHandler(mHandler);
        }
        
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
                case android.R.id.home: {
                    getSupportFragmentManager().popBackStack();
                }
                    break;
                case R.id.mnu_config: {
                    // 設定画面
                    Intent intent = new Intent(this, ConfigActivity.class);
                    startActivityForResult(intent, SystemConsts.REQUEST_CONFIG);
                }
                    break;
                case R.id.mnu_search: {
                    onSearchRequested();
                }
                    break;
                case R.id.mnu_playback_tab:{
                    if(Build.VERSION.SDK_INT <= 10){
                        
                        return false;
                    }
                }break;
                default: {
                }
            }
            return super.onOptionsItemSelected(item);
        } finally {
            // Google Analyticsにトラックイベントを送信
        }
    }

    @Override
    IMediaPlayerServiceCallback getCallBack() {
        return mCallback;
    }

    @Override
    ViewCache getViewCache() {
        return mViewCache;
    }

    @Override
    Handler getHandler() {
        return mHandler;
    }
}
